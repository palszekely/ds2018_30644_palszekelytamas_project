
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<style type="text/css">



body{
background-color: lightblue;
background-size:cover;
font-family:Arial;
text-align:center;
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

li {
    float: left;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

/* Change the link color to #111 (black) on hover */
li a:hover {
    background-color: #111;
}

</style>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>


</head>
<body>


<ul>
<li> <a href="listcars">Cars</a></li>
<li> <a href="search">Search</a></li>

</ul>

 

<br>
        <div align="center">
            <h1>Rentable Cars List</h1>
            <table border="1">
            <tr>
                <th>Brand</th>
                <th>City</th>
             
            </tr> 
            <c:forEach  items="${carList}" var="mycar" >
                <tr>
                   <td>${mycar.category}</td>
                   <td>${mycar.city}</td>
                    <td> <a href="getcarinfo?id=${mycar.id}&category=${mycar.category}&city=${mycar.city}&madeyear=${mycar.madeyear}&price=${mycar.price}&speed=${mycar.speed}&photo=${mycar.photo}">See details</a></td>
                        <td> <a href="rent?carid=${mycar.id}&username=${sessionScope.username}">Rent</a></td>
                </tr>
                </c:forEach>   
                      
            </table>
            
            
               <a href="myrents?username=${sessionScope.username}">My rented cars</a>
            
            
        </div>


</body>
</html>