package entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 import java.*;
 import java.io.Serializable;
import java.sql.Date;
@Entity
@Table(name = "rent_table")
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "carid")
    private int carid;
 
    @Column(name = "username")
    private String username;
    
	 @Column(name = "rent_date")
	    private Date rent_date;
    
    public Rent() {super();}

    
    public Rent(int id,String uname,Date md) {
    	this.username=uname;
    	this.carid=id;
    	this.rent_date=md;
    }
    
	public int getCarid() {
		return carid;
	}

	public void setCarid(int carid) {
		this.carid = carid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
   
  
}