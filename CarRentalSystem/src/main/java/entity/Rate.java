package entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 import java.*;
 import java.io.Serializable;
@Entity
@Table(name = "rate_table")
public class Rate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "carid")
    private int carid;
 
    @Column(name = "uname")
    private String username;
    
    @Column(name = "rate")
    private int rate;
    
    
    public Rate() {super();}
    
    public Rate(int cid,String n,int r) {this.carid=cid;this.rate=r;this.username=n;}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getCarid() {
		return carid;
	}

	public void setCarid(int carid) {
		this.carid = carid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}
    
}