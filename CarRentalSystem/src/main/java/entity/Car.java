package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "car_table")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
 
    @Column(name = "category")
    private String category;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "photo")
    private String photo;
    

	@Column(name = "madeyear")
    private int madeyear;
    
    @Column(name = "speed")
    private int speed;
    
    @Column(name = "price")
    private int price;
    
    @Column(name = "rented")
    private int rented;
    
    public Car() {super();}
    
    public Car(int id,String c,String ci,String p,int my,int sp,int pr) {
    	this.id=id;
    	this.category=c;
    	this.city=ci;
    	this.photo=p;
    	this.madeyear=my;
    	this.speed=sp;
    	this.price=pr;
    	
    }
 
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getMadeyear() {
		return madeyear;
	}

	public void setMadeyear(int madeyear) {
		this.madeyear = madeyear;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getRented() {
		return rented;
	}

	public void setRented(int rented) {
		this.rented = rented;
	}

    
    
    
    
}

