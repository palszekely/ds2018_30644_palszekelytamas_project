package dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.*;
import org.hibernate.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Car;
import entity.User;


import org.hibernate.SharedSessionContract;
@Repository
@Transactional
public class CarDao  {
	
	    @Autowired
	    SessionFactory session;
	 
	    
	    public boolean saveOrUpdate(Car car) {
	        session.getCurrentSession().saveOrUpdate(car);
	        return true;
	    }
	    
	    

		   public List<Car> listCars() {

		    	List<Car> carsList=new ArrayList<Car>() ;
		    	
		     carsList=session.getCurrentSession().createQuery("From Car Where rented=0").list();
		     
		    	return carsList;
		    	
		    }
		   
		   public List<Car> listCarsaboutcity(String city) {

		    	List<Car> carsList=new ArrayList<Car>() ;
		    	
		     Query q=session.getCurrentSession().createQuery("From Car Where rented=0 and city=:city");
		     q.setParameter("city", city);
		     carsList=q.list();
		     
		    	return carsList;
		    	
		    }
		   
		   public void updatestatus(int uid)
		    {
		    
		    	Query  q =  
		    			 session.getCurrentSession().createQuery("update Car c set c.rented = 1 where id=:carid"); 
		    	 q.setParameter("carid", uid); 
                
		    	q.executeUpdate();
		    
		    }
		   
		   public void updatestatus1(int uid)
		    {
		    
		    	Query  q =  
		    			 session.getCurrentSession().createQuery("update Car c set c.rented = 0 where id=:carid"); 
		    	q.setParameter("carid", uid); 
		    	q.executeUpdate();
		    
		    }
		   
		   public List<Car> mycars(List<Integer> carids){
			   
			   List<Car> carsList=new ArrayList<Car>() ;
			   
			   for(Integer i:carids) {
			   Query  q =  
		    			 session.getCurrentSession().createQuery("FROM Car  where id=:id "); 
		        q.setParameter("id", i); 
		        Car car = (Car) q.uniqueResult();
		        carsList.add(car);
		        
			   }
			   
			   return carsList;
		   }
		   
		   
		   
		   public int mycarsprice(List<Integer> carids){
                       List<Car> carsList=new ArrayList<Car>() ;
			   int price=0;
			   for(Integer i:carids) {
			   Query  q =  
		    			 session.getCurrentSession().createQuery("FROM Car  where id=:id "); 
		        q.setParameter("id", i); 
		        Car car = (Car) q.uniqueResult();
		       price+=car.getPrice();
		        
			   }
			   
			   return price;
			   
		    }
		   
		   
		   public boolean delete(int uid) {

				 Query query= session.getCurrentSession().createQuery("delete from Car where id=:vid ");
							query.setParameter("vid", uid);
							int i=query.executeUpdate();
					
							
							return true;
							
			    }
		   
	    
	    
}