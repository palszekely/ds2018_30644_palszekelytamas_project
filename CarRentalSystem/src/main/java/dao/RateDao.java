package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;




import entity.Rate;



@Repository
@Transactional

public class RateDao {
	
	@Autowired
    SessionFactory session;
 
    
    public boolean saveOrUpdate(Rate r) {
        session.getCurrentSession().saveOrUpdate(r);
        return true;
    }
    
    
    public boolean checkifexists(String un,int ci) {
    	
		 
		   Query  q =  
	    			 session.getCurrentSession().createQuery("FROM Rate  where uname=:username and carid=:carid "); 
	        q.setParameter("username", un); 
	        q.setParameter("carid", ci); 
	    	Object u = (Rate) q.uniqueResult();
			
			if (u==null) return false;
			else return true;
	       
     
    	
    	
    }
    

}
