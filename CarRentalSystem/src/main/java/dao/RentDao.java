package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Car;
import entity.Rent;


@Repository
@Transactional
public class RentDao  {
	
	    @Autowired
	    SessionFactory session;
	 
	    
	    public boolean saveOrUpdate(Rent r) {
	        session.getCurrentSession().saveOrUpdate(r);
	        return true;
	    }
	    
	    

		   public List<Integer> listCarsid(String username) {

		    	List<Integer> carsidList=new ArrayList<Integer>() ;
		    	List<Rent> rentList=new ArrayList<Rent>() ;
		    	
		    	Query query =session.getCurrentSession().createQuery("From Rent Where username=:username");
		    	query.setParameter("username", username);
		    	
		    	rentList=query.list();
		    	
		    	for(Rent r:rentList) {
		    		carsidList.add(r.getCarid());
		    	} 
		     
		     
		    	return carsidList;
		    	
		    }
		   
		   public List<Integer> listallCarsid() {

		    	List<Integer> carsidList=new ArrayList<Integer>() ;
		    	List<Rent> rentList=new ArrayList<Rent>() ;
		    	
		    	Query query =session.getCurrentSession().createQuery("From Rent");
		    	
		    	rentList=query.list();
		    	
		    	for(Rent r:rentList) {
		    		carsidList.add(r.getCarid());
		    	} 
		     
		     
		    	return carsidList;
		    	
		    }
		   
		   
		   public List<Integer> listcarids(java.sql.Date d) {
			   
			   List<Integer> carsidList=new ArrayList<Integer>() ;
		    	List<Rent> rentList=new ArrayList<Rent>() ;
		    	
		    	Query query =session.getCurrentSession().createQuery("From Rent where rent_date>:da");
		    	query.setParameter("da", d);
		    	
		    	rentList=query.list();
		    	
		    	for(Rent r:rentList) {
		    		carsidList.add(r.getCarid());
		    	} 
		     
		     
		    	return carsidList;
		        
		    }
		   
		   
		   
		   
		   public boolean delete(int uid) {

				 Query query= session.getCurrentSession().createQuery("delete from Rent where carid=:carid");
							query.setParameter("carid", uid);
							int i=query.executeUpdate();
					
							
							return true;
							
			    }
}