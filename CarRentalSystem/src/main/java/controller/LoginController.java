package controller;

import dao.UserDao;
import entity.User;
import service.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LoginController {
	
	@Autowired
    UserService userService;
	
	
	    @RequestMapping(value="/login",method=RequestMethod.GET)
	    public ModelAndView displayLogin(ModelMap modelMap,HttpSession session,HttpServletRequest request, HttpServletResponse response)
	    {
	    	
	        ModelAndView model = new ModelAndView("login");
	        User loginBean =new User();
	        model.addObject("loginBean", loginBean);
	        User u=checkCookie(request);
	       session.setAttribute("username",u.getUsername());
	        return model;
	    }
	  

	
	    @RequestMapping(value="/login",method=RequestMethod.POST)
		public ModelAndView executeLogin(ModelMap modelMap,HttpSession session, HttpServletRequest request, HttpServletResponse response, @ModelAttribute("loginBean")User loginBean)
		{
				ModelAndView model= null;
				long start=System.currentTimeMillis();
				try
				{
					    
						int isValidUser = userService.isValidUser(loginBean.getUsername());
						session.setAttribute("username", loginBean.getUsername());
						BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(); 
						System.out.println("Password: "+loginBean.getPass()+"  Crypted pass: "+userService.userpass(loginBean.getUsername())  );
						
						
						if(isValidUser==0 && (encoder.matches(loginBean.getPass(),userService.userpass(loginBean.getUsername()))))
						{
								System.out.println("User Login Successful");
								request.setAttribute("loggedInUser", loginBean.getUsername());
								long finish=System.currentTimeMillis()-start;
								System.out.println("The login was processed in "+finish +" Millisecond");
								return new ModelAndView("redirect:/welcome");
								
						}
						
						else if(isValidUser==1 &&  (encoder.matches(loginBean.getPass(),userService.userpass(loginBean.getUsername())))) {
							long finish=System.currentTimeMillis()-start;
							System.out.println("The login was processed in "+finish +" Millisecond");
							return new ModelAndView("redirect:/adminpart");
							
						}
						else 
						{
							    model = new ModelAndView("redirect:/invalid");
								request.setAttribute("message", isValidUser);
								
						}

				}
				catch(Exception e)
				{
						e.printStackTrace();
				}
				
				
			return model;
		}

	public User checkCookie(HttpServletRequest request) {
		
		Cookie []cookies=request.getCookies();
		String username="";
		for(Cookie ck:cookies) {
			if (ck.getName().equalsIgnoreCase("username"))
				username=ck.getValue();
		}
		User u=new User(username);
		return u;
		
	}
	
	
}