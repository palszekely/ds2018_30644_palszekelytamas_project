package controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import entity.Car;
import entity.Rent;
import service.CarService;
import service.RentService;

@Controller
public class RentController {
	

    @Autowired
    RentService rentServices;
    @Autowired
    CarService carServices;
    
/*
    @RequestMapping(value = "/rentcar", method = RequestMethod.GET)
    public ModelAndView getPage(HttpServletRequest request, HttpServletResponse response) {
    	 ModelAndView model = new ModelAndView("rentcar");
        return model;
    }
 */
    @RequestMapping(value = "/rent")
    public  ModelAndView getSaved(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid,@RequestParam String username) {
        Map<String, Object> map = new HashMap<String, Object>();
        ModelAndView mv = new ModelAndView("rentcar");
        carServices.updatestatus(carid);
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        Rent rent=new Rent(carid,username,date);
        
        if (rentServices.saveOrUpdate(rent)) {
            map.put("status", "200");
            map.put("message", "Your record have been saved successfully");
        }
      
        return mv;
    }
    
    
    @RequestMapping(value = "/myrents", method = RequestMethod.GET)
    public ModelAndView listcars(HttpServletRequest request, HttpServletResponse response ,@RequestParam String username,@ModelAttribute("mycar") Car car) {
       List<Integer> carsids=rentServices.listCarsid(username);
    	List<Car> carsList=carServices.mycars(carsids);
		ModelAndView model = new ModelAndView("myrentedcars");
		model.addObject("carList",carsList);
		return model;
    }
    
    @RequestMapping(value = "/admincarlist", method = RequestMethod.GET)
    public ModelAndView listcars1(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("mycar") Car car) {
       List<Integer> carsids=rentServices.listallCarsid();
    	List<Car> carsList=carServices.mycars(carsids);
		ModelAndView model = new ModelAndView("admincarlist");
		model.addObject("carList",carsList);
		return model;
    }
    
    @RequestMapping(value = "/unrent")
    public  ModelAndView getSaved1(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid) {
        Map<String, Object> map = new HashMap<String, Object>();
        ModelAndView mv = new ModelAndView("admincarlist");
        carServices.updatestatus1(carid);
        rentServices.delete(carid);
        
       
      
        return mv;
    }
	
	
    

}
