package controller;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;



import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
@Controller
public class WelcomeController {
	

	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView listContact(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView("welcome");
			
		 
		    return model;
    }
	
	@RequestMapping(value = "/adminpart", method = RequestMethod.GET)
    public ModelAndView listContact1(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView("adminpart");
			
		 
		    return model;
    }
	

	
	

}