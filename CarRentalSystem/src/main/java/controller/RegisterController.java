package controller;
	 
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.ResponseBody;
	import org.springframework.web.servlet.ModelAndView;

import entity.User;
import service.UserService;
	 


	@Controller
public class RegisterController {
	

	    @Autowired
	    UserService userServices;
	 
	    @RequestMapping(value = "/register", method = RequestMethod.GET)
	    public ModelAndView getPage(HttpServletRequest request, HttpServletResponse response) {
	    	 ModelAndView model = new ModelAndView("register");
			 User usersdao=new User();
			 model.addObject("userss", usersdao);
	        return model;
	    }
	 

	    @RequestMapping(value = "/register", method = RequestMethod.POST)
	  	    public  ModelAndView getSaved(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userss") User users) {
	  	        Map<String, Object> map = new HashMap<String, Object>();  
	  	        if (userServices.yetexist(users.getUsername())==-1)
	  	        {
	  	        	return new ModelAndView("redirect:/invalid");
	  	        }
	  	        else
	  	        {
	  	        	BCryptPasswordEncoder pe=new BCryptPasswordEncoder();
	  	        	String pass=users.getPass();
	  	        	String cryptedpass=pe.encode(pass);
	  	        	users.setPass(cryptedpass);
	  	        if (userServices.saveOrUpdate(users)) {
	  	            map.put("status", "200");
	  	            map.put("message", "Your record have been saved successfully");
	  	        }
	  	 
	  	        return new ModelAndView("redirect:/login");
	  	        }
	  	    }
	 
	
}