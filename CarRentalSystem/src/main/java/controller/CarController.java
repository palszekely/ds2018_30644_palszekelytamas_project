package controller;
	 
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
	import org.springframework.web.servlet.ModelAndView;

import auxiliary.DatePicker;
import auxiliary.MyFileWriter;
import auxiliary.Search;
import entity.Car;
import service.CarService;
import service.RentService;
	 


	@Controller
public class CarController {
	

	    @Autowired
	    CarService carServices;
	    @Autowired
	    RentService rentServices;
	 
	    @RequestMapping(value = "/caradd", method = RequestMethod.GET)
	    public ModelAndView getPage(HttpServletRequest request, HttpServletResponse response) {
	    	 ModelAndView model = new ModelAndView("caradd");
			 Car cardao=new Car();
			 model.addObject("carmodel", cardao);
	        return model;
	    }
	    
	    @RequestMapping(value = "/search", method = RequestMethod.GET)
	    public ModelAndView getPageone(HttpServletRequest request, HttpServletResponse response) {
	    	 ModelAndView model = new ModelAndView("search");
			 Search s=new Search();
			 model.addObject("citymodel", s);
	        return model;
	    }
	    
	    @RequestMapping(value = "/search", method = RequestMethod.POST)
	    public  ModelAndView getSavedone(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("carmodel") Search s) {
	        Map<String, Object> map = new HashMap<String, Object>(); 
	        ModelAndView model = new ModelAndView("listcars");
	        List<Car> carsList= carServices.listCarsaboutcity(s.getCity());
	        model.addObject("carList",carsList);
	        return model;
	 
	    }
	 
	    @RequestMapping(value = "/caradd", method = RequestMethod.POST)
	    public  ModelAndView getSaved(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("carmodel") Car car) {
	        Map<String, Object> map = new HashMap<String, Object>();  
	        if (carServices.saveOrUpdate(car)) {
	            map.put("status", "200");
	            map.put("message", "Your record have been saved successfully");
	        }
	 
	        return new ModelAndView("redirect:/adminpart");
	    }
	    
	    @RequestMapping(value = "/listcars", method = RequestMethod.GET)
	    public ModelAndView listcars(HttpServletRequest request, HttpServletResponse response ,@ModelAttribute("mycar") Car car) {
	    	List<Car> carsList=carServices.listCars();
			ModelAndView model = new ModelAndView("listcars");
			model.addObject("carList",carsList);
			return model;
	    }
	    
	    @RequestMapping(value="/getcarinfo")
	    public ModelAndView getDeviceInfo(@RequestParam int id,@RequestParam String category, @RequestParam String city,@RequestParam String photo,@RequestParam Integer madeyear,@RequestParam Integer price,@RequestParam Integer speed){
	        ModelAndView mv = new ModelAndView("carinfo");
	        Car c = new Car(id,category,city,photo,madeyear,speed,price);
	        mv.addObject("car", c);
	        
	        return mv;
	    }
	    
	    
	    @RequestMapping(value = "/deletecar")
	    public  ModelAndView getSaved1(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid) {
	        Map<String, Object> map = new HashMap<String, Object>();
	        ModelAndView mv = new ModelAndView("admincarlist");
	        carServices.delete(carid);
	        
	       
	      
	        return mv;
	    }
	    
	    
	    @RequestMapping(value = "/adminlistcars", method = RequestMethod.GET)
	    public ModelAndView listcarsadmin(HttpServletRequest request, HttpServletResponse response ,@ModelAttribute("mycar") Car car) {
	    	List<Car> carsList=carServices.listCars();
			ModelAndView model = new ModelAndView("adminlistcars");
			model.addObject("carList",carsList);
			return model;
	    }
	    
	    @RequestMapping(value = "/datepicker", method = RequestMethod.GET)
	    public ModelAndView getPageone1(HttpServletRequest request, HttpServletResponse response) {
	    	 ModelAndView model = new ModelAndView("datepicker");
			 DatePicker s=new DatePicker();
			 model.addObject("mydatepicker", s);
	        return model;
	    }
	    
	    
	    @RequestMapping(value = "/datepicker", method = RequestMethod.POST)
	    public ModelAndView listdpickeradmin1(HttpServletRequest request, HttpServletResponse response ,@ModelAttribute("mydatepicker") DatePicker date ) throws ParseException {
	    	java.util.Date date1= new SimpleDateFormat("dd/MM/yyyy").parse(date.getDp());  
	    	java.sql.Date sDate = new java.sql.Date(date1.getTime());
	    	System.out.println(sDate);
	    	List<Integer> idlist=rentServices.listcarids(sDate);
	    	for(Integer i:idlist) {
	    		System.out.println(i);
	    	}
	    	int price=carServices.mycarsprice(idlist);
	    	System.out.println(price);
	    	MyFileWriter mf=new MyFileWriter();
	    	try {
				mf.MyFileWriterm(price);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ModelAndView model = new ModelAndView("adminpart");
		
			return model;
	    }
	  
	 
	
}