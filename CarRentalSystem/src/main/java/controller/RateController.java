package controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import entity.Rate;
import service.RateService;


@Controller
public class RateController {
	
	   @Autowired
	    RateService rateServices;
	   
	    @RequestMapping(value = "/rate1")
	    public  ModelAndView getSaved1(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid,@RequestParam String username) {

	        ModelAndView mv = new ModelAndView("succrate");
	        ModelAndView mv1 = new ModelAndView("insuccrate");
            boolean b=rateServices.checkifexists(username, carid);
            Rate mr=new Rate(carid,username,1);
            if (b==false) {
                 rateServices.saveOrUpdate(mr);
            	 return mv;
            }
            else {
            	 return mv1;
            }
  
	    }
	    @RequestMapping(value = "/rate2")
	    public  ModelAndView getSaved2(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid,@RequestParam String username) {

	        ModelAndView mv = new ModelAndView("succrate");
	        ModelAndView mv1 = new ModelAndView("insuccrate");
            boolean b=rateServices.checkifexists(username, carid);
            Rate mr=new Rate(carid,username,2);
            if (b==false) {
                 rateServices.saveOrUpdate(mr);
            	 return mv;
            }
            else {
            	 return mv1;
            }
  
	    }
	    @RequestMapping(value = "/rate3")
	    public  ModelAndView getSaved3(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid,@RequestParam String username) {

	        ModelAndView mv = new ModelAndView("succrate");
	        ModelAndView mv1 = new ModelAndView("insuccrate");
            boolean b=rateServices.checkifexists(username, carid);
            Rate mr=new Rate(carid,username,3);
            if (b==false) {
                 rateServices.saveOrUpdate(mr);
            	 return mv;
            }
            else {
            	 return mv1;
            }
  
	    }
	    @RequestMapping(value = "/rate4")
	    public  ModelAndView getSaved4(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid,@RequestParam String username) {

	        ModelAndView mv = new ModelAndView("succrate");
	        ModelAndView mv1 = new ModelAndView("insuccrate");
            boolean b=rateServices.checkifexists(username, carid);
            Rate mr=new Rate(carid,username,4);
            if (b==false) {
                 rateServices.saveOrUpdate(mr);
            	 return mv;
            }
            else {
            	 return mv1;
            }
  
	    }
	    @RequestMapping(value = "/rate5")
	    public  ModelAndView getSaved5(HttpServletRequest request, HttpServletResponse response, @RequestParam int carid,@RequestParam String username) {

	        ModelAndView mv = new ModelAndView("succrate");
	        ModelAndView mv1 = new ModelAndView("insuccrate");
            boolean b=rateServices.checkifexists(username, carid);
            Rate mr=new Rate(carid,username,5);
            if (b==false) {
                 rateServices.saveOrUpdate(mr);
            	 return mv;
            }
            else {
            	 return mv1;
            }
  
	    }


}
