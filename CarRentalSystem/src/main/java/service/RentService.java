package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.CarDao;
import dao.RentDao;
import entity.Car;
import entity.Rent;

@Service
@Transactional
public class RentService {
	
	  @Autowired
	    RentDao rentDao;
	    @Transactional
	    public boolean saveOrUpdate(Rent r) {
	        return rentDao.saveOrUpdate(r);
	    }
	    @Transactional
	    public List<Integer> listCarsid(String username) {
	    	return rentDao.listCarsid(username);
	    }
	    
	    @Transactional
	    public List<Integer> listallCarsid() {
	    	return rentDao.listallCarsid();
	    }
	    
	    @Transactional
	    public boolean delete(int i) {
	        return rentDao.delete(i);
	    }
	    
	    @Transactional
	    public List<Integer> listcarids(java.sql.Date d) {
	        return rentDao.listcarids(d);
	    }
	
	

}
