package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.RateDao;
import entity.Rate;



@Service
@Transactional
public class RateService {
	
	 @Autowired
	    RateDao rateDao;
	    @Transactional
	    public boolean saveOrUpdate(Rate rate) {
	        return rateDao.saveOrUpdate(rate);
	    }
	    
	    @Transactional
	    public boolean checkifexists(String uname,int carid) {
	        return rateDao.checkifexists(uname,carid);
	    }

}
