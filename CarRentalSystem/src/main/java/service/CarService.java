package service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.CarDao;
import dao.UserDao;
import entity.Car;
import entity.User;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class CarService {
	
	    @Autowired
	    CarDao carDao;
	    @Transactional
	    public boolean saveOrUpdate(Car car) {
	        return carDao.saveOrUpdate(car);
	    }
	    
	    @Transactional
	    public boolean delete(int car) {
	        return carDao.delete(car);
	    }
	    
	    @Transactional
	    public List<Car> listCars() {
	    	return carDao.listCars();
	    }
	    
	    @Transactional
	    public List<Car> listCarsaboutcity(String s) {
	    	return carDao.listCarsaboutcity(s);
	    }
	    
	    @Transactional
	    public void updatestatus(int carid) {
	    	 carDao.updatestatus(carid);
	    }
	    
	    @Transactional
	    public void updatestatus1(int carid) {
	    	 carDao.updatestatus1(carid);
	    }
	    
	    @Transactional
	    public List<Car> mycars(List<Integer> carids){
	    	return carDao.mycars(carids);
	    }
	    
	    @Transactional
	    public int mycarsprice(List<Integer> carids){
	    	return carDao.mycarsprice(carids);
	    }
	   
	    
	
	   

}
