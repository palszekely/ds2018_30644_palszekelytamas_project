package service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import dao.UserDao;
import entity.User;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class UserService {
	
	    @Autowired
	    UserDao userDao;
	    @Transactional
	    public boolean saveOrUpdate(User users) {
	        return userDao.saveOrUpdate(users);
	    }
	    
	    public int isValidUser(String username)
	    {
	    	return userDao.isValidUser(username);
	    }
	    
	    public int yetexist(String username)
	    {
	    	return userDao.yetexist(username);
	    }
	    public String userpass(String username)
	    {
	    	return userDao.userpass(username);
	    }
	   

}
