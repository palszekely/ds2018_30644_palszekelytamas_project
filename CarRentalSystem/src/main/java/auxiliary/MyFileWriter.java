package auxiliary;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;



public class MyFileWriter {

	public MyFileWriter() {
		super();
	}


	public boolean MyFileWriterm(int price) throws IOException {
        BufferedWriter f = null;
        try {
            f = new BufferedWriter(new FileWriter("D:\\"+price+".txt"));
            f.write("The price of all rented autos for the specified interval is :"+price);
        }
        catch(IOException e) {
            System.out.println(e);
        }
        finally {
            if (f != null)
                f.close();  
        }
    	return true;
    }


}
